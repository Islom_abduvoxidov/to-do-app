import Header from "./components/Header/Header"
import ListTodo from "./components/todo-item/Todo-item";


function App() {
  return (
    <div className="App">
      <section className="todo">
        <ListTodo />
      </section>
    </div>
  );
}

export default App;
