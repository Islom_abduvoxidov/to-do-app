import React, { useEffect, useState } from 'react'

function Done({ todos }) {
   const [done, setDone] = useState()
   console.log(done);

   useEffect(() => {
      setDone(todos.filter((e) => e.isComplite === true))
      // doneTodo.length;
   }, [todos])

   return (
      <div>
         <p className="length__todo">Done: { done ? done.length : 0 }</p>
      </div>
   )
}

export default Done;
