import React from 'react'

function Search({ setSearchText }) {
   const handlerSearch = (e) => {
      const value = e.target.value;
      setSearchText(value)
   }
   return (
      <div>
         <input 
            type="search"
            placeholder="search"
            onChange={handlerSearch}
            className="todo__search"
            type="text" />
      </div>
   )
}

export default Search
