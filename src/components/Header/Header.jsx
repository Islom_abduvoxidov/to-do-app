import { useState, useEffect } from "react";
import "../../index.css";
import Search from "../filters/Search";

function Header({ todos, setList}) {
   const [searchText, setSearchText] = useState('')
   // console.log(list);
   

   useEffect(() => {
      let result = todos.filter((todo) => {
         return todo.text.includes(searchText)
      })
      // console.log('result', result);
      setList(result)
   }, [searchText])
   
   return(
      <div className="todo__top">
         <div className="todo__header">
            <h1 className="todo__title">Todo List</h1>
            <Search setSearchText={setSearchText} />
         </div>
      </div>
   )
}

export default Header;