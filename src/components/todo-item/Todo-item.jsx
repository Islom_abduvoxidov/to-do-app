import "../../index.css"
import { useState, useEffect } from "react";
import Header from "../Header/Header";
import Done from "../Done/Done";

function ListTodo() {
   const [todos, setTodos] = useState([]);
   const [count, setCount] = useState(0);
   const [list, setList] = useState([]);
   // console.log();

   useEffect(() => {
      setList(todos)
   }, [todos])
   
   const handlerTodo = (e) => {
      if (e.code === 'Enter') {
         const todosArray = {
            id: new Date().getTime(),
            text: e.target.value,
            isComplite: false,
         }
         
         setTodos([todosArray, ...todos]);
         e.target.value = null;
      }
   }

   const countValue = (evn) => {
      setCount(evn.target.value.length);
   }
   
   return(
      <>
      <Header todos={todos} setList={setList} />
      <div className="todo__render">
      
      <ul className="todo__list" >
      {
         list.length > 0 ? list.map((todo, i) => {
            return(
               <li
               key={todo.id}
               datatype={todo.id}
               className={todo.isComplite === true ? "todo__item complite" : "todo__item"}>
                  <input type="checkbox"
                  datatype={todo.id}
                  onChange={() => {
                  setTodos(
                     todos.map((item) => {
                        if (item.id === todo.id) {
                           return {
                              ...item, isComplite: !item.isComplite,
                           }
                        }
                        return item;
                     })
                     )
                  }}
                  />
                  
                  <p className={todo.isComplite === true ? "todo__text line" : "todo__text"}>
                  <span className="index">{i + 1}: </span>
                  {todo.text}
                  </p>
                  <button
                  onClick={() => {
                     setTodos(todos.filter((del) => del.id !== todo.id))
                  }
               }
               className="todo__delete"
               >Delete</button>
               </li>
               )
            })
            : <span className="not__found">there is nothing here...</span>
         }
         </ul>

         <input 
            onKeyUp={handlerTodo}
            onChange={countValue}
            placeholder="type your text..."
            className="todo__input"
            type="text" />
         <p className="todo__counter-value"><span>count:</span> {count}</p>
         
         <div className="todo__bottom">
            <p className="length__todo">All: {todos.length}</p>
            <Done todos={ todos }/>
         </div>

         </div>
      </>
   )
}

export default ListTodo;